// express.js

let express = require("express");
	//load express module to be able to use express properties and methods.

const PORT = 4000
	// store port in a variable.


// create a server - express();
	// app is now the server
let app = express();


// middleware-body parser
// setup for allowing the server to handle data from requests(client)
app.use(express.json());
	// allow your app to read json data
app.use(express.urlencoded({extended:true}));
	// allow your app to read data from the forms

// how to insert routes

app.get("/", (req, res) => res.send(`Bye World`));

// mini activity- show a message that say "Hello" in hello endpoint

app.get("/hello", (req, res) => res.send(`Hello from the Hello Endpoint`));

// mini-activity- create a greeting route that will send request data to the server 
// display dynamic message depending on the value of the request data. Message should be: "Hello there Joy!"
// If data changed, the response message should display the corresponding changed value
// example: "Hello there, Miah"


app.post("/greetings", (req, res) =>{ 

	console.log(req.body);
		// {firstName: "Jungkook"}
	res.send(`Hello there, ${req.body.firstName}`);
});

app.listen(PORT, () =>	console.log(`Server running at port ${PORT}`));


