// node.js

let http = require("http");
const port = 4000;

http.createServer((req, res) => {
	res.writeHead(200, {"Content-Type" : "text/plain"});
	res.write("Bye World!");
	res.end();
}).listen(port);

console.log("Connected.")